// 页面适应
var winW = $(window).width();
var winH = $(window).height();
var headH = $(".head").outerHeight(true);

function skipAdaption() {
  $(".shareLink").css({
    "height": winH,
    "background": "#f6f7f2"
  });
  $(".skipBg").css({
    "height": winH
  });
}

function mainAdaption() {
  $(".main").css({
    "height": winH - headH
  });
}

function applyAdaption() {
  var topH = $(".top").outerHeight(true);
  var centerH = $(".center").outerHeight(true);
  // $(".top").css({
  //   "padding-top": headH + 20
  // });
  // $(".center").css({
  //   "top": headH + topH + 20
  // });
  $(".bottom").css({
    "padding-top": topH,
    "height": winH - headH - topH
  });
}